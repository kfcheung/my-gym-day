package com.kfcheung.mytesttwo;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.Toolbar;

import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


import java.util.ArrayList;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

public class ListFragment extends Fragment {
    private Spinner spinner;
    DatabaseReference databaseReference;
    ListView listView;
    String SelectDis;
    ArrayList<String> arrayList = new ArrayList<>();
    ArrayAdapter<String> arrayAdapter;
    String[] dis_cn = {"中西區", "東區", "離島區", "九龍城區", "葵青區", "觀塘區", "北區", "西貢區", "沙田區", "深水埗區", "南區", "大埔區", "荃灣區", "屯門區", "灣仔區", "黃大仙區", "油尖旺區", "元朗區"};

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_list, container, false);

        databaseReference = FirebaseDatabase.getInstance().getReference("centreContact");
        listView = v.findViewById(R.id.listview);
        spinner = v.findViewById(R.id.spinner);

        ArrayAdapter<String> disAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, dis_cn);
        disAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(disAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
                arrayList.clear();
                //arrayAdapter.notifyDataSetChanged();
                SelectDis = parent.getItemAtPosition(position).toString();
                getCentreListOne(SelectDis);
                //Toast.makeText(getActivity(), "Selected: " + SelectDis, Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        arrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, arrayList);


        return v;
    }

    void getCentreListOne(String selectDis) {
        Toast.makeText(getActivity(), "waiting to get data", Toast.LENGTH_SHORT).show();
        databaseReference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                String value = dataSnapshot.getValue(SportCentreContact.class).toString();
                String value1 = dataSnapshot.getValue(SportCentreContact.class).getCentre(selectDis);
                if (!value1.equals("")) {
                    arrayList.add(value1);
                    arrayAdapter.notifyDataSetChanged();

                    listView.setAdapter(arrayAdapter);
//                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//                        @Override
//                        public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
//                            ListView listView = (ListView) arg0;
//                            String value = listView.getItemAtPosition(arg2).toString();
//                            //Toast.makeText(getActivity(),"ID：" + arg3 +"   選單文字：" + value,Toast.LENGTH_LONG).show();
//
//                            //viewFacility();
//                        }
//
//                    });
                }

            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }


}
