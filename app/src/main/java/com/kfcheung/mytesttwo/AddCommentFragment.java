package com.kfcheung.mytesttwo;

import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.auth.data.model.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

public class AddCommentFragment extends Fragment {
    Button btn_send;
    EditText ed_title,ed_desc;
    FirebaseUser user;
    private DatabaseReference mDatabase;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_add_comment, container, false);
        btn_send = v.findViewById(R.id.btn_send);
        ed_title = v.findViewById(R.id.ed_title);
        ed_desc = v.findViewById(R.id.ed_desc);
        mDatabase = FirebaseDatabase.getInstance().getReference("GymBuddy");
        user = FirebaseAuth.getInstance().getCurrentUser();

        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                writeNewComment(ed_desc.getText().toString(),ed_title.getText().toString());
            }
        });

        return v;

    }


    private void writeNewComment(String desc, String title) {
        String username = user.getDisplayName();
        DiscussionGroup comment = new DiscussionGroup(desc,title,username);
        mDatabase.push().setValue(comment);
        getGroupFragment();


    }

    void getGroupFragment(){
        Fragment fragment = new DiscussionGroupFragment();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, fragment ); // give your fragment container id in first parameter
        transaction.addToBackStack(null);  // if written, this transaction will be added to backstack
        transaction.commit();
    }
}
