package com.kfcheung.mytesttwo;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.zip.Inflater;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

public class LocationFragment extends Fragment implements OnMapReadyCallback {
    private static final String MAPVIEW_BUNDLE_KEY = "9001";
    private MapView mapView;
    private static final LatLng MeiLamSportsCentre = new LatLng(22.379029,114.175577);
    private static final LatLng YuenChauKokSportsCentre = new LatLng(22.379837, 114.204328);
    private static final LatLng YuenWoRoadSportsCentre = new LatLng(22.382981, 114.192538);
    private static final LatLng MaOnShanSportsCentre = new LatLng(22.426015, 114.229532);
    private static final LatLng HinKengSportsCentre = new LatLng(22.363195, 114.171326);
    private static final LatLng HengOnSportsCentre = new LatLng(22.416932, 114.227866);
    ArrayList<LatLng> markerList = new ArrayList<>();
    ArrayList<String> nameList = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_place, container, false);
        mapView = view.findViewById(R.id.map);
        markerList.add(MeiLamSportsCentre);
        markerList.add(YuenChauKokSportsCentre);
        markerList.add(YuenWoRoadSportsCentre);
        markerList.add(MaOnShanSportsCentre);
        markerList.add(HinKengSportsCentre);
        markerList.add(HengOnSportsCentre);
        nameList.add("Mei Lam Sports Centre");
        nameList.add("Yuen Chau Kok SportsCentre");
        nameList.add("Yuen Wo Road SportsCentre");
        nameList.add("Ma On Shan Sports Centre");
        nameList.add("Hin Keng Sports Centre");
        nameList.add("Heng On Sports Centre");

        initGoogleMap(savedInstanceState);
        return view;
    }

    private void initGoogleMap(Bundle savedInstanceState){
        Bundle mapViewBundle = null;
        if (savedInstanceState != null) {
            mapViewBundle = savedInstanceState.getBundle(MAPVIEW_BUNDLE_KEY);
        }

        mapView.onCreate(mapViewBundle);

        mapView.getMapAsync(this);
    }
    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onStart() {
        super.onStart();
        mapView.onStart();
    }
    @Override
    public void onStop() {
        super.onStop();
        mapView.onStop();
    }
    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    1);
        }

        for (int i=0;i<markerList.size();i++){
            googleMap.addMarker(new MarkerOptions().position(markerList.get(i)).title(nameList.get(i)));
        }
        //googleMap.addMarker(new MarkerOptions().position(new LatLng(22.379029,114.175577)).title("here"));
        googleMap.setMyLocationEnabled(true);
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(22.3736,114.179),15));
    }
    @Override
    public void onPause() {
        mapView.onPause();
        super.onPause();
    }

    @Override
    public void onDestroy() {
        mapView.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }
}


