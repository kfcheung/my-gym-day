package com.kfcheung.mytesttwo;

public class DiscussionGroup {
    String username;
    String title;
    String desc;

    public DiscussionGroup() {
    }

    public DiscussionGroup(String desc, String title, String username) {
        this.username = username;
        this.title = title;
        this.desc = desc;
    }

    @Override
    public String toString() {
        return username + "\n\n\n" +
                title + "\n\n" +
                desc;

    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
