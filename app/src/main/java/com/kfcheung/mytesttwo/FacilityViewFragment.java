package com.kfcheung.mytesttwo;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;



public class FacilityViewFragment extends Fragment {
    DatabaseReference databaseReference;
    ArrayList<String> arrayList = new ArrayList<>();
    ArrayList<String> centreList = new ArrayList<>();
    ArrayAdapter<String> centreAdapter;
    ArrayAdapter<String> arrayAdapter;
    String selectCentre;
    ListView listView;
    Spinner spinner;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_facility_view, container, false);
        listView = v.findViewById(R.id.facility_list);
        spinner = v.findViewById(R.id.spinner);
        centreAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, centreList);
        databaseReference = FirebaseDatabase.getInstance().getReference("facility");
        arrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, arrayList);
        centreList.add("Choose Sport Centre");
        centreAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(centreAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
                arrayList.clear();
                selectCentre = parent.getItemAtPosition(position).toString();
                getFacility(selectCentre);
                //Toast.makeText(getActivity(), "Selected: " + selectCentre, Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        getCentre();
        //Toast.makeText(getActivity(), "waiting to get data", Toast.LENGTH_SHORT).show();
        return v;
    }

    void getCentre(){
        databaseReference.addChildEventListener(new ChildEventListener() {
            String centre = "";
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                String name_centre = dataSnapshot.getValue(SportCentreFacility.class).getName_cn();
                if(!name_centre.equals(centre)){
                    centre = name_centre;
                    centreList.add(name_centre);
                    centreAdapter.notifyDataSetChanged();
                    centreAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinner.setAdapter(centreAdapter);

                }

            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    void getFacility(String selectCentre){
        String cen = "";
        cen = selectCentre;
        databaseReference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                //String value = dataSnapshot.getValue(SportCentreFacility.class).toString();
                String name_centre = dataSnapshot.getValue(SportCentreFacility.class).getSelectCentreFacility(selectCentre);
                //Toast.makeText(getActivity(),name_centre,Toast.LENGTH_SHORT).show();
                if (!name_centre.equals("")) {
                    arrayList.add(name_centre);
                    arrayAdapter.notifyDataSetChanged();
                }


                    listView.setAdapter(arrayAdapter);


            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }
}
