package com.kfcheung.mytesttwo;

public class SportCentreFacility {
    String Equipment_cn;
    String Equipment_en;
    String Name_cn;
    String Name_en;
    String No_of_set;
    String Room_No;
    String Shared_with_persons_with_disabilities;


    public SportCentreFacility() {
    }

    public SportCentreFacility(String equipment_cn, String equipment_en, String name_cn, String name_en, String no_of_set, String room_No, String shared_with_persons_with_disabilities) {
        Equipment_cn = equipment_cn;
        Equipment_en = equipment_en;
        Name_cn = name_cn;
        Name_en = name_en;
        No_of_set = no_of_set;
        Room_No = room_No;
        Shared_with_persons_with_disabilities = shared_with_persons_with_disabilities;
    }

    @Override
    public String toString() {
        return
                "Equipment : " + Equipment_cn + "\n" +
                "Name : " + Name_cn + "\n" +
                "No_of_set : " + No_of_set + "\n";
    }

    public String getSelectCentreFacility(String selectCentre){
        if(selectCentre.equals(Name_cn)){
            return  "設施 : " + Equipment_cn + "\n"
                    + "Equipment : " + Equipment_en + "\n\n"
                    + "數量 : " + No_of_set + "\n"
                    + "No. of set : " + No_of_set + "\n";
        }
        return "";
    }

    public String getEquipment_cn() {
        return Equipment_cn;
    }

    public void setEquipment_cn(String equipment_cn) {
        Equipment_cn = equipment_cn;
    }

    public String getEquipment_en() {
        return Equipment_en;
    }

    public void setEquipment_en(String equipment_en) {
        Equipment_en = equipment_en;
    }

    public String getName_cn() {
        return Name_cn;
    }

    public void setName_cn(String name_cn) {
        Name_cn = name_cn;
    }

    public String getName_en() {
        return Name_en;
    }

    public void setName_en(String name_en) {
        Name_en = name_en;
    }

    public String getNo_of_set() {
        return No_of_set;
    }

    public void setNo_of_set(String no_of_set) {
        No_of_set = no_of_set;
    }

    public String getRoom_No() {
        return Room_No;
    }

    public void setRoom_No(String room_No) {
        Room_No = room_No;
    }

    public String getShared_with_persons_with_disabilities() {
        return Shared_with_persons_with_disabilities;
    }

    public void setShared_with_persons_with_disabilities(String shared_with_persons_with_disabilities) {
        Shared_with_persons_with_disabilities = shared_with_persons_with_disabilities;
    }
}
