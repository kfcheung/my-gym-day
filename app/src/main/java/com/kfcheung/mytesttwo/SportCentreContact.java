package com.kfcheung.mytesttwo;

public class SportCentreContact {
    String District_en;
    String District_cn;
    String Name_en;
    String Name_cn;
    String Address_en;
    String Address_cn;
    String Phone;
    String GIHS;
    String Size_en;
    String Size_cn;
    String Longitude;
    String Latitude;

    public SportCentreContact() {
    }

    public SportCentreContact(String district_en, String district_cn, String name_en, String name_cn, String address_en, String address_cn, String phone, String GIHS, String size_en, String size_cn, String longitude, String latitude) {
        District_en = district_en;
        District_cn = district_cn;
        Name_en = name_en;
        Name_cn = name_cn;
        Address_en = address_en;
        Address_cn = address_cn;
        Phone = phone;
        this.GIHS = GIHS;
        Size_en = size_en;
        Size_cn = size_cn;
        Longitude = longitude;
        Latitude = latitude;
    }

    @Override
    public String toString() {
        return
                "District :" + District_cn+ "\n" +
                "Name:" + Name_cn + '\n' +
                "Address:" + Address_cn + '\n' +
                "Phone :" + Phone + '\n';
    }

    public String getCentre(String selectDis){
        String result = "";
        if(selectDis.equals(District_cn)){
            result = Name_cn+"\n地址:"+Address_cn+"\n電話:"+Phone;
        }
        return result;
    }


    public String getGIHS() {
        return GIHS;
    }

    public void setGIHS(String GIHS) {
        this.GIHS = GIHS;
    }

    public String getSize_en() {
        return Size_en;
    }

    public void setSize_en(String size_en) {
        Size_en = size_en;
    }

    public String getSize_cn() {
        return Size_cn;
    }

    public void setSize_cn(String size_cn) {
        Size_cn = size_cn;
    }

    public String getLongitude() {
        return Longitude;
    }

    public void setLongitude(String longitude) {
        Longitude = longitude;
    }

    public String getLatitude() {
        return Latitude;
    }

    public void setLatitude(String latitude) {
        Latitude = latitude;
    }

    public String getDistrict_en() {
        return District_en;
    }

    public void setDistrict_en(String district_en) {
        District_en = district_en;
    }

    public String getDistrict_cn() {
        return District_cn;
    }

    public void setDistrict_cn(String district_cn) {
        District_cn = district_cn;
    }

    public String getName_en() {
        return Name_en;
    }

    public void setName_en(String name_en) {
        Name_en = name_en;
    }

    public String getName_cn() {
        return Name_cn;
    }

    public void setName_cn(String name_cn) {
        Name_cn = name_cn;
    }

    public String getAddress_en() {
        return Address_en;
    }

    public void setAddress_en(String address_en) {
        Address_en = address_en;
    }

    public String getAddress_cn() {
        return Address_cn;
    }

    public void setAddress_cn(String address_cn) {
        Address_cn = address_cn;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }
}
